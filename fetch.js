// fetch('https://jsonplaceholder.typicode.com/users').then((Response)=>Response.json())
const fs = require("fs");
const path = require("path");

// 1. Fetch all the users
function fetchData(api) {
    return fetch(api).then((data) => {
        if (data.ok) {
            return (data.json())
        } else {
            throw new Error(data.status)
        }
    }).then((data) => {
        fs.writeFile(path.join(__dirname, 'output.json'), JSON.stringify(data), (err, data) => {
            if (err) {
                throw new Error("error while writing")
            } else {
                console.log(data); data
            }
        })
    }).catch((err) => {
        console.log(err);
    })

}

//fetchData('https://jsonplaceholder.typicode.com/users')
// 2. Fetch all the todos

function fetchDataToDo(api) {
    return fetch(api).then((data) => {
        if (data.ok) {
            return (data.json())
        } else {
            throw new Error(data.status)
        }
    }).then((data) => {
        fs.writeFile(path.join(__dirname, 'output1.json'), JSON.stringify(data), (err, data) => {
            if (err) {
                throw new Error("error while writing")
            } else {
                console.log(data);
            }
        })
    }).catch((err) => {
        console.log(err);
    })

}

fetchDataToDo('https://jsonplaceholder.typicode.com/todos')


// // 3. Use the promise chain and fetch the users first and then the todos.

function DataFromBothApi(Api1, Api2) {
    return new Promise(() => {
        fetchData(Api1)
    }).then(() => {
        return fetchDataToDo(Api2)
    }).catch((err) => {
        throw new Error(err)
    })

}

DataFromBothApi('https://jsonplaceholder.typicode.com/users', 'https://jsonplaceholder.typicode.com/todos')












